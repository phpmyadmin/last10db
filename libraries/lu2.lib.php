<?php
/**
 * Get a last used db entries
 *
 * @uses    PMA_getRelationsParam()
 * @uses    PMA_backquote()
 * @uses    PMA_sqlAddslashes()
 * @uses    PMA_DBI_fetch_result()
 * @param   string   $username  the username
 * @return  array    list of last used db
 * @access  public
 */
function PMA_getDBHistory($username) {
    $cfgRelation = PMA_getRelationsParam();
    if (! $cfgRelation['dbhistorywork']) {
        return false;
    }
    $query = '
        SELECT `db`
        FROM ' . PMA_backquote($cfgRelation['db']) . '.' . PMA_backquote($cfgRelation['dbhistory']) . '
        WHERE `username` = \'' . PMA_sqlAddslashes($username) . '\'
        ORDER BY `timevalue` DESC
        LIMIT 0,10';

    return PMA_DBI_fetch_result($query, null, null, $GLOBALS['controllink']);
} // end of 'PMA_getDBHistory()' function

/**
 * Set a last used db entry
 *
 * @uses    PMA_getRelationsParam()
 * @uses    PMA_query_as_controluser()
 * @uses    PMA_backquote()
 * @uses    PMA_sqlAddslashes()
 * @param   string   $db        the name of the db
 * @param   string   $username  the username
 * @access  public
 */
function PMA_setDBHistory($db, $username) {
    $cfgRelation = PMA_getRelationsParam();
    if (! $cfgRelation['dbhistorywork']) {
        return false;
    }
    PMA_query_as_controluser('
        DELETE FROM
            ' . PMA_backquote($cfgRelation['db']) . '.' . PMA_backquote($cfgRelation['dbhistory']) . '
        WHERE `username` = \'' . PMA_sqlAddslashes($username) . '\' AND `db` = \'' . PMA_sqlAddslashes($db) . '\'
    ');
    PMA_query_as_controluser('
        INSERT INTO
            ' . PMA_backquote($cfgRelation['db']) . '.' . PMA_backquote($cfgRelation['dbhistory']) . '
            (`username`, `db`, `timevalue`)
        VALUES
            (\'' . PMA_sqlAddslashes($username) . '\',
            \'' . PMA_sqlAddslashes($db) . '\',
            NOW())'
    );
} // end of 'PMA_setDBHistory()' function

/**
 * Print last used dbs
 *
 * @uses    PMA_getDBHistory()
 * @uses    PMA_getDbLink()
 * @access  public
 */
function PMA_printDBHistory() {
    $dbhistory = PMA_getDBHistory($GLOBALS['cfg']['Server']['user']);
    if (! $dbhistory) return;
    echo '<div class="group">';
    echo '<h2>' . __('Last Used Databases') . '</h2>';
    echo '<ul>';
    foreach ($dbhistory as $line) {
        echo '<li>' . PMA_getDbLink($line) . '</li>';
    }
    echo '</ul>';
    echo '</div>';
} // end of 'PMA_printDBHistory()' function
?>
